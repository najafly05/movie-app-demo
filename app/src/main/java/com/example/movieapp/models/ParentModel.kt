package com.example.movieapp.models

data class ParentModel(
    val title: String,
    val movieList: List<MovieItem?>
)
