package com.example.movieapp.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.databinding.ParentRvLayoutBinding
import com.example.movieapp.models.ParentModel
import com.example.movieapp.ui.fragment.HomeFragmentDirections

class ParentAdapter(var context: Context, var parentModelList: List<ParentModel>): RecyclerView.Adapter<ParentAdapter.ViewHolder>() {
    inner class ViewHolder(var binding: ParentRvLayoutBinding) : RecyclerView.ViewHolder(binding.root)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentAdapter.ViewHolder {
        val binding = ParentRvLayoutBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(binding)
    }


    override fun onBindViewHolder(holder: ParentAdapter.ViewHolder, position: Int) {
        val category = parentModelList[position]
        val myHolder = holder.binding
        myHolder.tvParentTitle.text = category.title
        val childAdapter = ChildAdapter(context, category.movieList)
        myHolder.rvChild.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        myHolder.rvChild.adapter = childAdapter
        childAdapter.notifyDataSetChanged()

    }

    override fun getItemCount(): Int {
        return parentModelList.size
    }
}

