package com.example.movieapp.models


import com.google.gson.annotations.SerializedName

data class PopularMovieResponse(
    @SerializedName("results")
    val popularMovieItems: List<MovieItem?>?
)