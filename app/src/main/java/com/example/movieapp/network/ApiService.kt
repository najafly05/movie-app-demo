package com.example.movieapp.network

import com.example.movieapp.models.NowPlayingMovieResponse
import com.example.movieapp.models.PopularMovieResponse
import com.example.movieapp.models.UpComingMovieResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface ApiService {
    @GET("popular")
    suspend fun getPopularMovieList(@Header("Authorization") token: String): Response<PopularMovieResponse>

    @GET("now_playing")
    suspend fun getNowPlayingMovieList(@Header("Authorization") token: String): Response<NowPlayingMovieResponse>

    @GET("upcoming")
    suspend fun getUpComingMovieList(@Header("Authorization") token: String) : Response<UpComingMovieResponse>
}