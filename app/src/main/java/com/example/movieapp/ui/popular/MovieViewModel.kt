package com.example.movieapp.ui.popular

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movieapp.models.MovieItem
import com.example.movieapp.network.ApiClient
import com.example.movieapp.utils.Constants
import kotlinx.coroutines.launch
import java.lang.Exception

class MovieViewModel: ViewModel() {
    val popularMovies: MutableLiveData<List<MovieItem?>?> = MutableLiveData()
    val nowPlayingMovies: MutableLiveData<List<MovieItem?>?> = MutableLiveData()
    val upComingMovies: MutableLiveData<List<MovieItem?>?> = MutableLiveData()

    val isLoading = MutableLiveData(false)
    val errorMessage: MutableLiveData<String?> = MutableLiveData()

    fun getPopularMovieList() {
        isLoading.value = true

        viewModelScope.launch {
            try {
                val response =
                    ApiClient.getClient().getPopularMovieList(token = Constants.BEARER_TOKEN)

                if (response.isSuccessful ){
                    popularMovies.postValue(response.body()?.popularMovieItems)
                } else {
                    if (response.message().isNullOrEmpty()) {
                        errorMessage.value = "An unknown error occured"
                    } else {
                        errorMessage.value = response.message()
                    }
                }

            } catch (e: Exception) {
                errorMessage.value = e.message
            }
            finally {
                isLoading.value = false
            }
        }
    }
    fun getNowPlayingMovieList() {
        isLoading.value = true

        viewModelScope.launch {
            try {
                val response =
                    ApiClient.getClient().getNowPlayingMovieList(token = Constants.BEARER_TOKEN)

                if (response.isSuccessful ){
                    nowPlayingMovies.postValue(response.body()?.nowPlayingMovieItems)
                } else {
                    if (response.message().isNullOrEmpty()) {
                        errorMessage.value = "An unknown error occured"
                    } else {
                        errorMessage.value = response.message()
                    }
                }

            } catch (e: Exception) {
                errorMessage.value = e.message
            }
            finally {
                isLoading.value = false
            }
        }
    }
    fun getUpcomingMovieList() {
        isLoading.value = true

        viewModelScope.launch {
            try {
                val response =
                    ApiClient.getClient().getUpComingMovieList(token = Constants.BEARER_TOKEN)

                if (response.isSuccessful ){
                    upComingMovies.postValue(response.body()?.upcomingMovieItems)
                } else {
                    if (response.message().isNullOrEmpty()) {
                        errorMessage.value = "An unknown error occured"
                    } else {
                        errorMessage.value = response.message()
                    }
                }

            } catch (e: Exception) {
                errorMessage.value = e.message
            }
            finally {
                isLoading.value = false
            }
        }
    }
}