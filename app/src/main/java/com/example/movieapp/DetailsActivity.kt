package com.example.movieapp

import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.movieapp.databinding.ActivityDetailsBinding
import com.example.movieapp.models.MovieItem

class DetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailsBinding
    private lateinit var movie: MovieItem
    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val intent = intent
        if (intent != null && intent.hasExtra("movie")) {
            movie = intent.getParcelableExtra("movie", MovieItem::class.java)!!
        }
        /*val movieIv = binding.imageView
        Glide.with(this)
            .load(movie.posterPath)
            .override(700, 700) // Genişlik ve yüksekliği burada belirleyin
            .into(movieIv)*/


        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}