package com.example.movieapp.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.DetailsActivity
import com.example.movieapp.databinding.ChildRvLayoutBinding
import com.example.movieapp.models.MovieItem
import com.example.movieapp.utils.loadImage

class ChildAdapter(var context: Context, var childModelList: List<MovieItem?>): RecyclerView.Adapter<ChildAdapter.ViewHolder>() {
    inner class ViewHolder(var binding: ChildRvLayoutBinding) : RecyclerView.ViewHolder(binding.root)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildAdapter.ViewHolder {
        val binding = ChildRvLayoutBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ChildAdapter.ViewHolder, position: Int) {
        val movie = childModelList[position]
        val myHolder = holder.binding
        myHolder.imageViewChildItem.loadImage(movie?.posterPath)
        myHolder.imageViewChildItem.setOnClickListener {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra("movie", movie)
            context.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return childModelList.size
    }
}