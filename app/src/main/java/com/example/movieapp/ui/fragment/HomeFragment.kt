package com.example.movieapp.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.movieapp.databinding.FragmentHomeBinding
import com.example.movieapp.models.MovieItem
import com.example.movieapp.ui.adapter.ParentAdapter
import com.example.movieapp.models.ParentModel
import com.example.movieapp.ui.popular.MovieViewModel

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private lateinit var parentModelList: ArrayList<ParentModel>

    private val viewModel by viewModels<MovieViewModel>()
    private lateinit var parentAdapter: ParentAdapter


    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(layoutInflater, container, false)


        parentModelList = ArrayList()

        observeEvents()
        with(binding.rvParent) {
            setHasFixedSize(true)
            val divider = DividerItemDecoration(context, LinearLayoutManager(context).orientation)
            addItemDecoration(divider)
        }

        viewModel.getPopularMovieList()
        viewModel.getNowPlayingMovieList()
        viewModel.getUpcomingMovieList()

        return binding.root

    }

    private fun observeEvents() {
        viewModel.errorMessage.observe(viewLifecycleOwner) { error ->
            binding.textViewHomeError.text = error
            binding.textViewHomeError.isVisible = true
        }

        viewModel.isLoading.observe(viewLifecycleOwner) { loading ->
            binding.progressBar.isVisible = loading
        }

        // Bu kısımda popüler filmleri beklemeye başlayın
        viewModel.popularMovies.observe(viewLifecycleOwner) { popularList ->
            // Popüler filmler geldiğinde, şimdi sıradaki "Now Playing" filmlerini bekleyin
            viewModel.nowPlayingMovies.observe(viewLifecycleOwner) { nowPlayingList ->
                // "Now Playing" filmler geldiğinde, son olarak "Upcoming" filmleri bekleyin
                viewModel.upComingMovies.observe(viewLifecycleOwner) { upcomingList ->
                    // Şimdi tüm veriler geldi, güncelleyebilirsiniz
                    val mergedList = ArrayList<ParentModel>()

                    if (popularList.isNullOrEmpty() && nowPlayingList.isNullOrEmpty() && upcomingList.isNullOrEmpty()) {
                        binding.textViewHomeError.text = "There are no movies :("
                        binding.textViewHomeError.isVisible = true
                    } else {
                        if (!popularList.isNullOrEmpty()) {
                            mergedList.add(ParentModel("Popular Movies", popularList))
                        }

                        if (!nowPlayingList.isNullOrEmpty()) {
                            mergedList.add(ParentModel("Now Playing", nowPlayingList))
                        }

                        if (!upcomingList.isNullOrEmpty()) {
                            mergedList.add(ParentModel("Upcoming", upcomingList))
                        }

                        parentModelList.clear()
                        parentModelList.addAll(mergedList)

                        // Set the adapter and layout manager outside the if block
                        parentAdapter = ParentAdapter(requireContext(), parentModelList)
                        binding.rvParent.layoutManager = LinearLayoutManager(requireContext())
                        binding.rvParent.adapter = parentAdapter
                    }
                }
            }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}