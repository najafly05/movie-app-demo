package com.example.movieapp.utils

class Constants {
    companion object{
        const val BASE_URL = "https://api.themoviedb.org/3/movie/"
        const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500/"
        const val BEARER_TOKEN = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI4MTA1YzQxZWIwYzE2NDU0ZDYwYmM1ODBiN2Y2NTk0OCIsInN1YiI6IjY1MGYxNjNjNmY1M2UxMGFhYzJhNWIxMyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.VtssOfI5GS8x9yZUZbheMVrc9v1K59AMv7xP71uwrqE"
    }
}